<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## О проекте

Авто-дилерский сервис _**CAR4YOU**_ - мое первое веб-приложение на laravel.

(выполнялся в рамках прохождения испытательного срока в QSOFT)


## Stack

- php ^8.1
- laravel ^9.0
- vue ^2.0

